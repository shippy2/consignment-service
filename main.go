package main

import (
	pb "csmt-service/proto/consignment"
	"log"
	"os"

	vessel "csmt-service/proto/vessel"

	micro "github.com/micro/go-micro"
)

const (
	DEFAULT_MGO_ADDR = "localhost:27017"
)

func main() {

	dbAddr := os.Getenv("DB_HOST")

	if dbAddr == "" {
		dbAddr = DEFAULT_MGO_ADDR
	}

	session, err := CreateSession(dbAddr)

	defer session.Close()
	if err != nil {
		log.Fatalf("create session error: %v\n", err)
	}

	server := micro.NewService(
		micro.Name("consignment"),
	)

	server.Init()

	vesselClient := vessel.NewVesselService("vessel", server.Client())
	pb.RegisterShippingServiceHandler(server.Server(), &handler{session, vesselClient})

	if err := server.Run(); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}
