module csmt-service

go 1.13

replace sigs.k8s.io/yaml => github.com/kubernetes-sigs/yaml v1.1.0

require (
	github.com/golang/protobuf v1.3.2
	github.com/micro/go-micro v1.18.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22

)
