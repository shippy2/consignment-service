FROM golang:alpine as builder

RUN apk --no-cache add git

WORKDIR /app

COPY . .

ENV GO111MODULE=on
ENV GOPROXY=https://goproxy.cn,direct

RUN go mod download

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o consignment-service

# ==

FROM alpine:latest

RUN mkdir /app

WORKDIR /app

# ADD consignment-service /app/consignment-service
COPY --from=builder /app/consignment-service .

CMD ["./consignment-service"]
