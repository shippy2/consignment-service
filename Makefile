build:
	protoc -I proto proto/consignment/consignment.proto --micro_out=proto --go_out=proto
	protoc -I proto proto/vessel/vessel.proto --micro_out=proto --go_out=proto

docker:
	docker build -t consignment-service .

run:
	docker run -p 50051:50051 \
	-e MICRO_SERVER_ADDRESS=:50051 \
	-e MICRO_REGISTRY=mdns \
	consignment-service
dev:
	make build && make docker
