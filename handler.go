package main

import (
	"context"
	vessel "csmt-service/proto/vessel"
	"log"

	pb "csmt-service/proto/consignment"

	"gopkg.in/mgo.v2"
)

type handler struct {
	session      *mgo.Session
	vesselClient vessel.VesselService
}

// GetRepo ...
func (h *handler) GetRepo() Repository {
	return &ConsignmentRepository{h.session.Clone()}
}

// CreateConsignment ...
func (h *handler) CreateConsignment(ctx context.Context, req *pb.Consignment, resp *pb.Response) error {
	defer h.GetRepo().Close()

	spec := &vessel.Specification{
		Capacity:  int32(len(req.Containers)),
		MaxWeight: req.Weight,
	}
	rsp, err := h.vesselClient.FindAvailable(context.Background(), spec)
	if err != nil {
		return err
	}

	log.Printf("found vessel: %s\n", rsp.Vessel.Name)
	req.VesselId = rsp.Vessel.Id

	err = h.GetRepo().Create(req)
	if err != nil {
		return err
	}

	resp.Created = true
	resp.Consignment = req

	return nil
}

// GetConsignments ...
func (h *handler) GetConsignments(ctx context.Context, req *pb.GetRequest, resp *pb.Response) error {
	defer h.GetRepo().Close()

	cons, err := h.GetRepo().GetAll()
	if err != nil {
		return err
	}

	resp.Consignments = cons
	return nil
}

func (h *handler) Hello(ctx context.Context, req *pb.HelloRequest, resp *pb.HelloResponse) error {
	resp.Greeting = "Hello " + req.Name
	return nil
}
